﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinSamle
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            //UriImageSource im = (UriImageSource)ImageSource
            //    .FromUri(new Uri("https://www.santevet.com/upload/admin/images/article/chaton%20adoption.jpg"));
            //var im = ImageSource.FromResource("XamarinSamle.Images.chaton.jpg");
            ////im.CacheValidity = TimeSpan.FromDays(365);
            //IM_1.Source = im;
        }

        private void OnClick(object sender, EventArgs e)
        {
            App.Current.MainPage
                .Navigation.PushAsync(new SecondePage());
        }

        private void OpenModal(object sender, EventArgs e)
        {
            App.Current.MainPage
                .Navigation.PushModalAsync(new ModalPage());
        }
    }
}
